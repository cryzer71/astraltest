import React, { Component } from 'react';
import {Header} from 'semantic-ui-react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import Button from 'material-ui/Button';
import Input from 'material-ui/Input';
//import AdvancedSearch  from './AdvancedSearch';
//import leftSearchVac from './leftSearchVac'


const styles = theme => ({
    button: {
        margin: theme.spacing.unit,
    },
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    input: {
        marginLeft: '20%',
        margin: theme.spacing.unit,
        width:'55%',
    },
});


class FormComp extends Component{
    constructor(props){
        super(props);
         this.state = {
            open: false,
        };
    }


    viewVacancies() {
        this.props.deleteVacan();
        this.props.onViewVacan(this.vacInput.value);
        this.vacInput.value = "";
    }
    render(){
        const classes = this.props.classes;
        return(
            <header>
                    <Header as='h2' textAlign={'center'}>Поиск вакансий</Header>
                    <Input
                        placeholder="Ищу"
                        className={classes.input}
                        inputRef={(input)=>{this.vacInput = input}}
                        inputProps={{
                            'aria-label': 'Description',
                        }} />
                    <Button raised color="primary" className={classes.button} onClick={this.viewVacancies.bind(this)}>Найти</Button>
                <Button  href="/" className={classes.button}>
                    Расширенный поиск
                </Button>

            </header>
        )
    }
}
FormComp.propTypes = {
    classes: PropTypes.object.isRequired,
};
export default withStyles(styles)(FormComp);
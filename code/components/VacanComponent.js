import React, { Component } from 'react';
import {Container} from 'semantic-ui-react';

let options = {
    month: 'long',
    day: 'numeric',
    timezone: 'UTC',
};

export default class VacanComponent extends Component{
    render() {
        return(
            <div>
                {this.props.NameOfVacancies.map((vacancies,index)=>(
                    <Container text key={index}>
                        <div className={'HeadOfVacName'}><a>{vacancies.name}</a></div>
                        <div className={'SalaryContainer'}>
                            {vacancies.salary!=null&&vacancies.salary.from!=null&&vacancies.salary.to!=null?
                                <div className={'salaryName'}>Зарплата {vacancies.salary.from} - {vacancies.salary.to} руб</div>:
                                vacancies.salary!=null&&vacancies.salary.from!=null?
                                    <div>Зарплата от {vacancies.salary.from} руб</div>:
                                    vacancies.salary!=null&&vacancies.salary.to!=null?
                                        <div>Зарплата до {vacancies.salary.to} руб</div>:<div>Зарплата неизвестна</div>}
                        </div>
                        <div className={'resposibility'}>{vacancies.snippet.responsibility}</div>
                        <div className={'requirement'}>{vacancies.snippet.requirement}</div>
                        <div className={'employer'}><a>{vacancies.employer.name}</a>{vacancies.employer.trusted==true?
                            <a className={"bloko-link"} target="_blank" href="https://feedback.hh.ru/article/details/id/5951">
                                &nbsp;&nbsp;&nbsp;&nbsp;✔
                            </a>
                            :null}
                        </div>
                        <div className={'areaAndDate'}>{vacancies.area.name}  &nbsp;&nbsp;•&nbsp;&nbsp;  {new Date(vacancies.created_at).toLocaleString("ru", options)}</div>
                        <br />
                        <br />
                    </Container>))}
            </div>
        )
    }}
export default function  vacancies(state=[],action) {
    if (action.type === 'VIEW_VACANCIES') {
        return [
            ...state,
            action.payload
        ];
    }
    else if(action.type === 'DELETE_VACANCIES')
    {
        return state=[];
    }
    return state;
}
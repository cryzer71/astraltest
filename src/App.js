import React, { Component } from 'react';
import axios from 'axios'
import { connect } from 'react-redux'
import FormComp  from './components/FormComp.js';
import VacanComponent  from './components/VacanComponent.js';

//Класс App
class App extends Component{
    constructor(props) {
        super(props);
        this.state = {
            isError: false,
            selectedRowSize: 20};
        }
    render(){
        return(
            <div>
                {/*Вызов компонента формы*/}
                <FormComp store={this.props.store}
                          onFindVac={this.props.onFindVac}
                          setLoader={this.props.setLoader}
                          deleteVacan={this.props.deleteVacan}
                          onViewVacan={this.props.onViewVacan}
                          deleteError={this.props.deleteError}/>
                <br />
                {/*Вызов компонента с вакансиями*/}
                <VacanComponent Errors={this.props.Error}
                                NameOfVacancies={this.props.NameOfVacancies}
                                Loaders={this.props.TactOfLoader} />
            </div>


        );
    }
}
export default connect(
    state => ({
        // Перееопределение редусеров
        NameOfVacancies:
        //Фильтр массива по зарплатам, названия, и компании вакансий для дополнительной выборки по вакансиям
            state.ReduceVacancies.filter((ReduceVacancies) =>{
            if (ReduceVacancies.salary!==null){
                if (ReduceVacancies.salary.from!==null && ReduceVacancies.salary.to!==null){
                    if (ReduceVacancies.name.includes(state.FilterVac[0].name) &&
                        (ReduceVacancies.salary.from >= +state.FilterVac[0].salaryFrom) &&
                        (ReduceVacancies.salary.to <= +state.FilterVac[0].salaryTo) &&
                        (ReduceVacancies.employer.name.includes(state.FilterVac[0].employer))) {
                        return ReduceVacancies
                    }
                }
                else if (ReduceVacancies.salary.from!==null) {
                    if (ReduceVacancies.name.includes(state.FilterVac[0].name) &&
                        (ReduceVacancies.salary.from>= (state.FilterVac[0].salaryFrom))&&
                        (ReduceVacancies.employer.name.includes(state.FilterVac[0].employer))) {
                        return ReduceVacancies
                    }
                }
                else if (ReduceVacancies.salary.to!==null){
                    if (ReduceVacancies.name.includes(state.FilterVac[0].name) &&
                        (ReduceVacancies.salary.to<= +state.FilterVac[0].salaryTo) &&
                        (ReduceVacancies.employer.name.includes(state.FilterVac[0].employer))) {
                        return ReduceVacancies
                    }
                }
            }
            else{
                if( ReduceVacancies.name.includes(state.FilterVac[0].name)&&
                    ReduceVacancies.employer.name.includes(state.FilterVac[0].employer)) {
                    return ReduceVacancies
                }
            }
}),
        Filter: state.FilterVac,
        TactOfLoader: state.Loaders,
        Error: state.Errors
    }),
    dispatch => ({
        //Загрузка вакансий в хранилище
        onViewVacan: (VacanName,OnlySalary)=>{
            //Функция проверки статуса запроса по вакансиям
            function status(response) {
                if (response.status === 200) {
                    return Promise.resolve(response)
                } else {
                    dispatch({type: 'SET_ERROR'});
                    return Promise.reject(new Error(response.statusText))
                }
            }
            //Функция возвращения json response.data
            function json(response) {
                return response.data;
            }
            //axios get запрос по вакансиям по имени и чексбоксу(только с зарплатами)
            axios.get('https://api.hh.ru/vacancies?text='+VacanName+'&only_with_salary='+OnlySalary+'&per_page=50&page=0')
                .then(status)
                .then(json)
                .then(function(data) {
                            if (data.items.length===0){
                                dispatch({type: 'SET_ERROR'});
                            }
                            else{
                                 dispatch({type: 'VIEW_VACANCIES', payload: data.items});
                                 dispatch({type: 'DELETE_ERROR'});
                            }
                    dispatch({ type: 'DELETE_LOADER'});
                }).catch(function(error) {
                    console.log('По вашему запроcу больше ничего не надено', error);

            });
        },
        //Удаление вакансий из хранилища
        deleteVacan: ()=>{
            dispatch({ type: 'DELETE_VACANCIES'});
        },
        //Включить лоудер
        setLoader: () =>{
            dispatch({ type: 'SET_LOADER',payload: 1});
        },
        //Загрузка данных фильтра для вакансий
        onFindVac: (name,salaryfrom,salaryto,employer) => {
            let payloads={
                name: name,
                salaryFrom: salaryfrom,
                salaryTo: salaryto,
                employer: employer
            };
            dispatch({type: 'FIND_VAC', payload: payloads});
        },
        deleteError: ()=>{
            dispatch({type: 'DELETE_ERROR'});
        }
    }))(App);
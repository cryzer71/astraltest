import React, { Component } from 'react';
import {Container} from 'semantic-ui-react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import TextField from 'material-ui/TextField';
import { FormGroup,FormControlLabel } from 'material-ui/Form';
import Switch from 'material-ui/Switch';
import Button from 'material-ui/Button';
import axios from 'axios'
import { connect } from 'react-redux'
import Input from 'material-ui/Input';
import NumberFormat from 'react-number-format';
import Left from './LeftSearchVac.js';
import {styles} from '../constant/styleAdvancedSearch';

//Number Format для input  с зарплатами
function NumberFormatCustom(props) {
    return (
        <NumberFormat
            {...props}
            onChange={(event, values) => {
                props.onChange({
                    target: {
                        value: values.value,
                    },
                });
            }}
            thousandSeparator
            suffix=" ₽"
        />
    );
}
//Компонент расширенного поиска
class AdvancedSearch extends Component{
    constructor(props) {
        super(props);
        this.state = {
            checkedA: true,
            Salary: '',
            City: '',
            VacanName: '',
            SearchClick: false,
            LeftSearch: false};
    }
    handleChange = name => (event, checked) => {
        this.setState({ [name]: checked });
    };
    handleChange = Salary => event => {
        this.setState({
            [Salary]: event.target.value,
        });
    };
    handleChange = City => event => {
        this.setState({
            [City]: event.target.value,
        });
    };
    handleChange = VacanName => event => {
        this.setState({
            [VacanName]: event.target.value,
        });
    };
    //Поиск вакансий по определенным полям
   Options(e) {
       //Поиск по кнопке и Enter
       if (e.keyCode === 13 ||  e.nativeEvent.which===1) {
           this.setState({SearchClick: true});
           //Если не пустые то выполнить функции запроса
           if (this.state.Salary !== "" || this.state.City !== "")
           {
               this.setState({LeftSearch:true});
               this.setState({SearchClick: true});
               this.props.deleteVacan();
               this.props.setLoader();
               this.props.onViewVacan(this.state.VacanName, this.state.Salary, this.state.checkedA, this.state.City);
               this.setState({SearchClick: false});
           }
           else{
               this.setState({LeftSearch:false});
               this.props.deleteVacan();
           }
       }
   }

    render(){
       const classes = this.props.classes;
       //Переменные для инициализации элементов
       let SalaryTag,CityTag,leftSearch;
       //Если поиск, то дополнительный поиск
        if (this.state.LeftSearch){
            leftSearch = <Left onFindVac={this.props.onFindVac}/>;
        }
        //Обработка ошибок с пустым вводом(разные поля)
        if (this.state.City==="" && this.state.SearchClick===true){
            CityTag = <TextField
                error
                onKeyUp={this.Options.bind(this)}
                label="Город"
                placeholder='Калуга'
                className={classes.textField}
                margin="normal"
                value={this.state.City}
                onChange={this.handleChange('City')}
            />}
        else{
            CityTag = <TextField
                onKeyUp={this.Options.bind(this)}
                label="Город"
                placeholder='Калуга'
                className={classes.textField}
                margin="normal"
                value={this.state.City}
                onChange={this.handleChange('City')}
            />}
        if (this.state.Salary==="" && this.state.SearchClick===true) {
            SalaryTag = <Input
                onKeyUp={this.Options.bind(this)}
                error
                value={this.state.Salary}
                onChange={this.handleChange('Salary')}
                inputComponent={NumberFormatCustom}
                className={classes.input}
                label="Зарплата"
                margin="normal"
                placeholder='Зарплата'
                inputProps={{
                    'aria-label': 'Description',
                }}
            />
        }
        else {
            SalaryTag = <Input
                onKeyUp={this.Options.bind(this)}
                value={this.state.Salary}
                onChange={this.handleChange('Salary')}
                inputComponent={NumberFormatCustom}
                className={classes.input}
                label="Зарплата"
                margin="normal"
                placeholder='Зарплата'
                inputProps={{
                    'aria-label': 'Description',
                }}
            />
        }
        return(
            <div>
                    <header>
                        <Container text>
                            <div style={{fontSize:'1em',marginLeft: '30%'}}>
                                <span style={{color:'black',marginRight: '25px'}}>Вакансия:</span>
                                <TextField
                                    onKeyUp={this.Options.bind(this)}
                                    label="Вакансия"
                                    placeholder='front-end'
                                    className={classes.textField}
                                    margin="normal"
                                    value={this.state.VacanName}
                                    onChange={this.handleChange('VacanName')}
                                />
                            </div>
                            <div style={{fontSize:'1em',marginLeft: '30%'}}>
                                <span style={{color:'black',marginRight: '50px'}}>Город:</span>
                                {CityTag}
                            </div>
                            <br />
                            <div style={{fontSize:'1em',marginLeft: '30%',marginBottom: '15px'}}><span style={{color:'black',marginRight: '20px'}}>Уровень З/П:</span>
                                {SalaryTag}
                            </div>
                        </Container>
                    </header>
                    <div style={{backgroundColor: '#777'}}>
                             <FormGroup style={{marginLeft: '40%'}}>
                            <FormControlLabel
                            control={
                                <Switch
                                    checked={this.state.checkedA}
                                    onChange={(event, checked) => this.setState({ checkedA: checked })}
                                />
                                    }
                            label="Скрыть вакансии без указания зарплаты"
                                />
                            </FormGroup>
                </div>
                <div style={{backgroundColor: '#fff'}}>
                <Button raised style={{marginLeft: '48%',marginTop: '5px',marginBottom: '5px',backgroundColor: '#00e4e1'}} className={classes.button} onClick={this.Options.bind(this)}><b>Найти</b></Button>
                </div>
                    {leftSearch}
            </div>
        )
    }
}
AdvancedSearch.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default connect(
    state => ({
        Loaders: state.Loader,
    }),
    dispatch => ({
        //Запрос на поиск вакансий и на получения id города по названию и занесение в хранилище
        onViewVacan: (VacanName,Salary,OnlySalary,City)=>{
            let CityID;
            axios.get('https://api.hh.ru/suggests/areas?text=' + City)
                .then(function (response) {
                    if (response.data.items[0]!==undefined) {
                        CityID = response.data.items[0].id;
                    }
                    else{
                        dispatch({type: 'SET_ERROR'});
                    }
                    function status(response) {
                        if (response.status === 200) {
                            return Promise.resolve(response)
                        } else {
                            dispatch({type: 'SET_ERROR'});
                            dispatch({ type: 'DELETE_LOADER'});
                            return Promise.reject(new Error(response.statusText))
                        }
                    }
                    function json(response) {
                        return response.data;
                    }
                    axios.get('https://api.hh.ru/vacancies?text='+VacanName+'&salary='+Salary+'&only_with_salary='+OnlySalary+'&area='+CityID+'&per_page=50&page=0')
                        .then(status)
                        .then(json)
                        .then(function(data) {
                                if (data.items.length===0){
                                    dispatch({type: 'SET_ERROR'});
                                }
                                else{
                                    dispatch({type: 'VIEW_VACANCIES', payload: data.items});
                                    dispatch({type: 'DELETE_ERROR'});
                                }
                            dispatch({ type: 'DELETE_LOADER'});
                        }).catch(function(error) {
                        dispatch({type: 'SET_ERROR'});
                        dispatch({ type: 'DELETE_LOADER'});
                        console.log('По вашему запроcу больше ничего не надено', error);

                    });

                })
                .catch(function(error) {
                    dispatch({type: 'SET_ERROR'});
                    dispatch({type: 'DELETE_LOADER'});
                    console.log('По вашему запроcу больше ничего не надено', error);
                });
        },
        //Сброс вакансий из хранилища
        deleteVacan: ()=>{
            dispatch({ type: 'DELETE_VACANCIES'});
        },
        //Установка лоудера
        setLoader: () =>{
            dispatch({ type: 'SET_LOADER',payload: 1});
        },
        //Поля для фильтрации
        onFindVac: (name,salaryfrom,salaryto,employer) => {
            let payloads={
                name: name,
                salaryFrom: salaryfrom,
                salaryTo: salaryto,
                employer: employer
            };
            dispatch({type: 'FIND_VAC', payload: payloads});
        }
    }))(withStyles(styles)(AdvancedSearch));
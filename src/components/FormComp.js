import React, { Component } from 'react';
import {Header} from 'semantic-ui-react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import Button from 'material-ui/Button';
import Input from 'material-ui/Input';
import AdvSearch from './AdvancedSearch';
import Left from './LeftSearchVac.js';
import { FormGroup,FormControlLabel } from 'material-ui/Form';
import Switch from 'material-ui/Switch';
import {styles} from '../constant/styleFromComp';

//Компонент хеадер с формами
class FormComp extends Component{
    constructor(props) {
        super(props);
        this.state = { isOpenedAdvSearh: false,
            SearchClick: false ,
            MainSearch: true,
            AdventSearch: true,
            checkedA: true};
    }
    //Функция обработки при обычном поиске и расширенном
    toggleState() {
        this.setState({ isOpenedAdvSearh: !this.state.isOpenedAdvSearh,
            MainSearch: !this.state.MainSearch,
            AdventSearch: !this.state.AdventSearch });
            this.setState({SearchClick: false});
            this.props.deleteVacan();
            this.props.deleteError();

    }
    //Функция обработки по нажатию кнопки или Enter для запроса вакансий
    viewVacancies(e) {
        if (e.keyCode === 13 || e.nativeEvent.which===1) {
            this.setState({SearchClick: true});
            this.props.deleteVacan();
            this.props.setLoader();
            this.props.onViewVacan(this.vacInput.value,this.state.checkedA);
            this.vacInput.value = "";
        }
    }
    render(){
        const classes = this.props.classes;
        //Переменные для вставки кода
        let mainSearch,dropdownBlock,leftSearch,AdvenSearch,Toogle;
        //Если основной поиск то задааются элементы для основного поиска
        if (this.state.MainSearch){
             mainSearch = <span style={{marginLeft: '-200px'}}><Input
                     onKeyUp={this.viewVacancies.bind(this)}
                     placeholder="Ищу"
                     className={classes.input}
                     inputRef={(input)=>{this.vacInput = input}}
                     inputProps={{
                         'aria-label': 'Description',
                     }} />
                 <Button raised style={{backgroundColor: '#00e4e1'}} className={classes.button} onClick={this.viewVacancies.bind(this)}><b>Найти</b></Button>
                </span>;
             Toogle = <div style={{backgroundColor: '#666',color:'white'}}>
                 <div style={{float: 'left',marginTop:'0.6em', marginRight: '4.1em',marginLeft: '1em'}}>Найди свою вакансию</div>
                     <FormGroup>
                             <FormControlLabel
                                 style={{typography: {color: '#fff'}}}
                                 control={
                                     <Switch
                                         style={{action: '#fff'}}
                                         checked={this.state.checkedA}
                                         onChange={(event, checked) => this.setState({ checkedA: checked })}
                                     />
                                 }
                                 label="Скрыть вакансии без указания зарплаты"
                             />
                    </FormGroup>
                </div>
        }
        //Если не расширенный поиск, то задается кнопка с надписью расширенный поиск
        if (this.state.AdventSearch){
            AdvenSearch = <Button  className={classes.button} onClick={this.toggleState.bind(this)} >
                Расширенный поиск
            </Button>
        }
        //Если расширенный, то кнопка с надписью обычный поиск
        else{
            AdvenSearch = <Button  className={classes.button} onClick={this.toggleState.bind(this)} >
               Обычный поиск
            </Button>
        }
        //Если Расширенный поиск, то задаются элементы для расширенного поиска
        if (this.state.isOpenedAdvSearh) {
            dropdownBlock =
                <AdvSearch store={this.props.store}  onFindVac={this.props.onFindVac}/>
        }
        //Если нажата клавиша поиска, то появлятся компонент дополнительного поиска
        if (this.state.SearchClick){
            leftSearch = <Left onFindVac={this.props.onFindVac}/>;
        }
        else{
            leftSearch="";
        }

        return(
            <header>
                <div style={{float:'left'}}>
                    <Header as='h2' style={{marginTop:'0.5em', marginLeft:'0.8em'}}>Поиск вакансий</Header>
                </div>
                {mainSearch}
                <span style={{float:'right'}}>
                    {AdvenSearch}
                </span>
                {Toogle}
                {leftSearch }
                {dropdownBlock}
            </header>
        )
    }
}
FormComp.propTypes = {
    classes: PropTypes.object.isRequired,
};
export default withStyles(styles)(FormComp);
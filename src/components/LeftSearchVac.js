import React,{ Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import classNames from 'classnames';
import Drawer from 'material-ui/Drawer';
import Divider from 'material-ui/Divider';
import IconButton from 'material-ui/IconButton';
import MenuIcon from 'material-ui-icons/Menu';
import ChevronLeftIcon from 'material-ui-icons/ChevronLeft';
import TextField from 'material-ui/TextField';
import '../style.css';
import Input from 'material-ui/Input';
import NumberFormat from 'react-number-format';
import {styles, drawerWidth} from '../constant/constantsForLeftSearch';

//Функция NumberFormat для поля с зарплатой
function NumberFormatCustom(props) {
    return (
        <NumberFormat
            {...props}
            onChange={(event, values) => {
                props.onChange({
                    target: {
                        value: values.value,
                    },
                });
            }}
            thousandSeparator
            suffix=" ₽"
        />
    );
}
//Компонент дополнительного поиска слева
class LeftSearchVac extends Component {
    state = {
        VacanName: "",
        SalaryFrom: '',
        SalaryTo: Infinity,
        EmployerName: "",
        open: false,
    };
    handleDrawerOpen = () => {
        this.setState({ open: true });
    };
    handleChange = SalaryFrom => event => {
        this.setState({
            [SalaryFrom]: (event.target.value!=="")?event.target.value:(''),
        });
    };
    handleChange = SalaryTo => event => {
        this.setState({
            [SalaryTo]: (event.target.value!=="")?event.target.value:Infinity,
        });
    };
    handleDrawerClose = () => {
        this.setState({ open: false });
    };
    handleChange = VacanName => event => {
        this.setState({
            [VacanName]: event.target.value,
        });
    };
    handleChange = EmployerName => event => {
        this.setState({
            [EmployerName]: event.target.value,
        });
    };
    //Поля для фильтра
    FindVac() {
        this.props.onFindVac(this.state.VacanName,this.state.SalaryFrom,this.state.SalaryTo,this.state.EmployerName);
    }
    render() {
        const { classes } = this.props;
        return (
            <div className={classes.root} style={{float: 'left'}}>
                <div className={classes.appFrame}>
                            <IconButton
                                color="contrast"
                                aria-label="open drawer"
                                onClick={this.handleDrawerOpen}
                                className={classNames(classes.menuButton, this.state.open && classes.hide)}
                            >
                            <MenuIcon />
                            </IconButton>
                    <Drawer
                        type="persistent"
                        classes={{
                            paper: classes.drawerPaper,
                        }}
                        open={this.state.open}>
                        <div className={classes.drawerInner}>
                            <div className={classes.drawerHeader}>
                                <span style={{fontSize:'15px'}}><b>Ускорь поиск вакансии</b></span>
                                <IconButton onClick={this.handleDrawerClose}>
                                    <ChevronLeftIcon />
                                </IconButton>
                            </div>
                            <Divider />
                            <div>
                                <TextField
                                    style={{marginLeft: '25px'}}
                                    label="Вакансия"
                                    placeholder='front-end'
                                    className={classes.textField}
                                    value={this.state.VacanName}
                                    onChange={this.handleChange('VacanName')}
                                    onKeyUp={this.FindVac.bind(this)}
                                />
                                <br />
                                <br />
                                <Input
                                    style={{marginLeft: '25px'}}
                                    value={this.state.SalaryFrom}
                                    onChange={this.handleChange('SalaryFrom')}
                                    inputComponent={NumberFormatCustom}
                                    className={classes.input}
                                    label="Зарплата от"
                                    margin="normal"
                                    placeholder='Зарплата от'
                                    inputProps={{
                                        'aria-label': 'Description',
                                    }}
                                    onKeyUp={this.FindVac.bind(this)}
                                />
                                <br />
                                <br />
                                <Input
                                    style={{marginLeft: '25px'}}
                                    value={this.state.SalaryTo}
                                    onChange={this.handleChange('SalaryTo')}
                                    inputComponent={NumberFormatCustom}
                                    className={classes.input}
                                    label="Зарплата до"
                                    margin="normal"
                                    placeholder='Зарплата до'
                                    inputProps={{
                                        'aria-label': 'Description',
                                    }}
                                    onKeyUp={this.FindVac.bind(this)}
                                />
                                <br />
                                <TextField
                                    style={{marginLeft: '25px'}}
                                    label="Компания"
                                    placeholder='Астрал'
                                    className={classes.textField}
                                    value={this.state.EmployerName}
                                    onChange={this.handleChange('EmployerName')}
                                    onKeyUp={this.FindVac.bind(this)}
                                />
                                <br />
                            </div>
                        </div>
                    </Drawer>
                </div>
            </div>
        );
    }
}
LeftSearchVac.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(LeftSearchVac);
import React, { Component } from 'react';
import {Container} from 'semantic-ui-react';
import { Button } from 'semantic-ui-react'
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import { Loader } from 'semantic-ui-react';
import {config1, config2} from "../constant/chartsConfig";
import {styles} from '../constant/styleVacanComp';
const ReactHighcharts = require('react-highcharts');

//Функция счетчика
function makeCounter() {
    let currentCount = 1;
    return function() {
        return currentCount++;
    };
}

let options = {
    month: 'long',
    day: 'numeric',
    timezone: 'UTC',
};
//Компонент для вакансий
class VacanComponent extends Component{
    constructor(props) {
        super(props);
        this.state = {
            currentPage: 1,
            todosPerPage: 5
        };
    }
    //При получение новых сво-в переход на страницу
    componentWillReceiveProps(){
            this.setState({
                currentPage: 1
            });
    }
handleClick(event) {
        config2.series[0].data=[];
        this.setState({
            currentPage: Number(event.target.id)
        });
}
//Функция графика для зарплат
Chart1Salary(todosPerPage,currentTodos){
    for (let i = 0; i < todosPerPage; i++) {
        if (currentTodos[i]!==undefined){
            if (currentTodos[i].salary !== null) {
                if (currentTodos[i].salary.from !== null && currentTodos[i].salary.to !== null) {
                    config1.series[0].data[i] = currentTodos[i].salary.from;
                    config1.series[1].data[i] = currentTodos[i].salary.to;
                }
                else if (currentTodos[i].salary.from !== null) {
                    config1.series[0].data[i] = currentTodos[i].salary.from;
                    config1.series[1].data[i] = [];
                }
                else if (currentTodos[i].salary.to !== null) {
                    config1.series[0].data[i] = currentTodos[i].salary.to;
                    config1.series[1].data[i] = [];
                }
                else {
                    config1.series[0].data[i] = [];
                    config1.series[1].data[i] = [];
                }
            }
            else {
                config1.series[0].data[i] = [];
                config1.series[1].data[i] = [];
            }
        }
        else{
            config1.series[0].data[i] = [];
            config1.series[1].data[i] = [];
        }
    }
}
//Функция графика для Компаний
Chart2Employer(currentTodos) {
    config2.series[0].data=[];
        for (let k=0,i = 0; i < currentTodos.length; i++, k++) {
            let trigger = true;
            let parametr;
            if (config2.series[0].data[k]===undefined){
                config2.series[0].data[k]={};
                config2.series[0].data[k].name="";
                config2.series[0].data[k].y=0;
            }
            if (currentTodos[i] !== undefined) {
                for (let j = 0; j < k; j++) {
                    if (currentTodos[i].employer.name === config2.series[0].data[j].name) {
                        parametr = j;
                        trigger = false;
                    }
                }
                if (trigger) {
                    config2.series[0].data[k].counter = makeCounter();
                    config2.series[0].data[k].name = currentTodos[i].employer.name;
                    config2.series[0].data[k].y = config2.series[0].data[k].counter() /currentTodos.length;
                }
                else {
                    config2.series[0].data[parametr].y = config2.series[0].data[parametr].counter() /currentTodos.length;
                    config2.series[0].data.splice(k, 1);
                   k--;
                }

            }
            else {
                config2.series[0].data=[];

            }

        }
    }

render() {
        const classes = this.props.classes;
        const { currentPage, todosPerPage } = this.state;
        const indexOfLastTodo = currentPage * todosPerPage;
        const indexOfFirstTodo = indexOfLastTodo - todosPerPage;
        const currentTodos = this.props.NameOfVacancies.slice(indexOfFirstTodo, indexOfLastTodo);
//Значения зарплат для графика
         this.Chart1Salary(todosPerPage,currentTodos);
 //Значения компаний для диаграммы
          this.Chart2Employer(currentTodos);
          const renderTodos = currentTodos.map((vacancies, index) => {
            return <div key={index}>
                <Container text  className={'ContentBlock'}>
                    <div className={'HeadOfVacName'}><a target="_blank" href={vacancies.alternate_url}><span style={{fontFamily: 'Roboto', color: '#b76000'}}>{vacancies.name}</span></a></div>
                    <div className={'SalaryContainer'}>
                        {vacancies.salary!=null&&vacancies.salary.from!=null&&vacancies.salary.to!=null?
                            <div className={'salaryName'}>Зарплата {vacancies.salary.from} - {vacancies.salary.to} руб</div>:
                            vacancies.salary!=null&&vacancies.salary.from!=null?
                                <div>Зарплата от {vacancies.salary.from} руб</div>:
                                vacancies.salary!=null&&vacancies.salary.to!=null?
                                    <div>Зарплата до {vacancies.salary.to} руб</div>:<div>Зарплата неизвестна</div>}
                    </div>
                    <div className={'resposibility'}>{vacancies.snippet.responsibility}</div>
                    <div className={'requirement'}>{vacancies.snippet.requirement}</div>
                    <div className={'employer'}><a target="_blank" href={vacancies.employer.alternate_url}><span style={{color: '#8a8a8a'}}>{vacancies.employer.name}</span></a>{vacancies.employer.trusted===true?
                        <a className={"bloko-link"} target="_blank" rel="noopener noreferrer" href="https://feedback.hh.ru/article/details/id/5951">
                            &nbsp;&nbsp;&nbsp;&nbsp;✔
                        </a>
                        :null}
                    </div>
                    <div className={'areaAndDate'}>{vacancies.area.name}  &nbsp;&nbsp;•&nbsp;&nbsp;  {new Date(vacancies.created_at).toLocaleString("ru", options)}</div>

                </Container>
                <br />
                <br />
            </div>;
          });
        const pageNumbers = [];
        for (let i = 1; i <= Math.ceil( this.props.NameOfVacancies.length/ todosPerPage); i++) {
            pageNumbers.push(i);
        }
        const renderPageNumbers = pageNumbers.map(number => {
            return (
                    <Button
                            key={number}
                             id={number}
                             onClick={this.handleClick.bind(this)}
                             style={{
                                 fontFamily: "'PT Sans', sans-serif",backgroundColor: '#00e4e1'}}
                             className={classes.button}>
                    {number}</Button>
            );
        });
        //Вывод если ошибка и нет лоудера
        if (this.props.Errors===1 && this.props.Loaders===0){return (
            <div>
                По вашему запроcу ничего не надено... Повторите поиск
            </div>)}
        else if (this.props.Loaders===1){
        return (
            <div>
                <Loader key={0} size='huge' style={{marginTop: '150px'}} active id={0}>Загрузка</Loader>
                <div>
                    {renderTodos}
                    <ul id="page-numbers">
                        {renderPageNumbers}
                    </ul>
                </div>
            </div>)

        }
        //Вывод если нет лоудера и есть вакансии
        else if(this.props.Loaders===0 && this.props.NameOfVacancies[0] !== undefined){
            return (
                <div>
                    <Loader key={0} disabled  inline='centered' id={0}/>
                    <div>
                        {renderTodos}
                        <ul id="page-numbers">
                            {renderPageNumbers}
                        </ul>
                    </div>
                    <div id="container" style={{float: 'left', width: '650px', height: '400px'}}>
                        <ReactHighcharts config={config1} />,
                    </div>
                    <div id="container" style={{float: 'right',width: '650px', height: '400px'}}>
                        <ReactHighcharts config={config2} />,
                    </div>
                </div>
            )
        }
        else{
            return <div></div>
        }

    }}

VacanComponent.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(VacanComponent);
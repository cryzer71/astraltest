const ReactHighcharts = require('react-highcharts');


export const config1 = {
    chart: {
        backgroundColor: '#fff'
    },
    title: {
        text: 'Минимальная и максимальная зарплата из поиска'
    },
    yAxis: {
        title: {
            text: 'Рубли'
        }
    },
    legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'middle'
    },

    plotOptions: {
        series: {
            label: {
                connectorAllowed: false
            },
            pointStart: 1
        }
    },

    series: [{
        name: 'Минимальная зарплата',
        data: []
    }, {
        name: 'Максимальная зарплата',
        data: []

    }
    ],

    responsive: {
        rules: [{
            condition: {
                maxWidth: 500
            },
            chartOptions: {
                legend: {
                    layout: 'horizontal',
                    align: 'center',
                    verticalAlign: 'bottom'
                }
            }
        }]
    }

};

export const config2 ={
    chart: {
        backgroundColor: '#fff',
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
    },
    title: {
        text: 'Отношение найденых вакансий'
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
    plotOptions: {
        area: {
            fillColor: '#f0f0f0'
        },
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: true,
                format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                style: {
                    color: (ReactHighcharts.theme && ReactHighcharts.theme.contrastTextColor) || 'black'
                }
            }
        }
    },
    series: [{
        name: 'Компания',
        colorByPoint: true,
        data: []
    }]
};
export const styles = theme => ({
    button: {
        margin: theme.spacing.unit,

    },
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    input: {
        marginLeft: '20%',
        margin: theme.spacing.unit,
        width:'55%',
    },
});
import React from 'react';
import ReactDOM from 'react-dom';
import './style.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import {Provider} from 'react-redux';
import {createStore} from 'redux';
import reducer from './reducers/index'

//Создание хранилища
export const store= createStore(reducer);
//Поиск элемента в котором все будет хранится
const elem = document.getElementById('App');
//Рендер
ReactDOM.render(
    <Provider store={store}>
        <App />
    </Provider>,
    elem
);
registerServiceWorker();
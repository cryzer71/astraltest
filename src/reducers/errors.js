export default function errors(state = 0, action) {
    if (action.type === 'SET_ERROR') {
        return state = 1;
    }
    else if (action.type === 'DELETE_ERROR') {
        return state = 0;
    }
    return state;
}
import { combineReducers } from 'redux'
import ReduceVacancies from './vacancies';
import Loaders from './loaders'
import Errors from './errors';
import FilterVac from './filterVac';

export default combineReducers({
    ReduceVacancies,
    Loaders,
    Errors,
    FilterVac
})
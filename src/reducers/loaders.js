export default function loaders(state = 0, action) {
    if (action.type === 'SET_LOADER') {
        return state = 1;
    }
    else if (action.type === 'DELETE_LOADER') {
        return state = 0;
    }
    return state;
}